package com.firebaserealtimepractice;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private Context mctx;
    List<UserModel> userModelList;

    public UserAdapter(Context mctx, List<UserModel> userModelList) {
        this.mctx = mctx;
        this.userModelList = userModelList;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mctx).inflate(R.layout.row_layout, parent, false);

        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        int dPosition = position;

        holder.tvName.setText(userModelList.get(dPosition).getName());
        holder.tvPassword.setText(userModelList.get(dPosition).getPassword());
    }

    @Override
    public int getItemCount() {
        return userModelList.size();
    }



    public class UserViewHolder extends RecyclerView.ViewHolder {
          TextView tvName;
          TextView tvPassword;

        public UserViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View rootView) {
            tvName = (TextView) rootView.findViewById(R.id.tv_name);
            tvPassword = (TextView) rootView.findViewById(R.id.tv_password);
        }
    }
}
