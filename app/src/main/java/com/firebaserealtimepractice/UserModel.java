package com.firebaserealtimepractice;

public class UserModel {
    String name;
    String password;

    public UserModel(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public UserModel() {
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
}
