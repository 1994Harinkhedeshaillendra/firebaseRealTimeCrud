package com.firebaserealtimepractice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    protected EditText etName;
    protected EditText etPassword;
    protected Button button;
    protected RecyclerView rv1;
    UserAdapter mAdapter;
    List<UserModel> userModelList = new ArrayList<>();
    DatabaseReference myRef;

    // Write a message to the database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);

      /*  DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World! Update");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });*/
        initView();
        myRef = FirebaseDatabase.getInstance().getReference("message");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(TextUtils.isEmpty(etName.getText()))) {
                    if (!(TextUtils.isEmpty(etPassword.getText()))) {
                        UserModel model = new UserModel(etName.getText().toString(), etPassword.getText().toString());
                            addUser(model);
                    } else {
                        etPassword.setError("please enter password");
                    }
                }else {
                    etName.setError("please enter name");
                }
            }
        });
    }

    private void addUser(UserModel model) {
         myRef = FirebaseDatabase.getInstance().getReference("message");
        String id = myRef.push().getKey();
        myRef.child(id).setValue(model);
       // myRef.setValue("Hello, World! Update");

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAllUser();
    }

    private void getAllUser(){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //on each change it will call

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    UserModel userModel = dataSnapshot1.getValue(UserModel.class);
                    userModelList.add(userModel);
                }

                mAdapter = new UserAdapter(MainActivity.this, userModelList);
                rv1.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        })   ;

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button) {

        }
    }

    private void initView() {
        etName = (EditText) findViewById(R.id.et_name);
        etPassword = (EditText) findViewById(R.id.et_password);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(MainActivity.this);
        rv1 = (RecyclerView) findViewById(R.id.rv_1);

        //setup recyclerview
        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        rv1.setItemAnimator(new DefaultItemAnimator());
    }
}
